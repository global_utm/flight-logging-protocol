# Repository moved
******
**August 2018: This repository has been migrated to Github. Please find the new repository location here: [https://github.com/gutma-org/flight-logging-protocol](https://github.com/gutma-org/flight-logging-protocol)**
******

### README ###

This protocol was approved for release to by GUTMA members on March 12 2018.  The protocol is open to public comments and contributions.

### Goals ###

The protocol's purpose is to harmonize flight telemetry data logs (such as GPS location, speed, and if available: battery voltage, drone identifier, battery identifier, payloads identifier).
It is targeted at UAV manufacturers. Logs can then be used by UAV pilots, third-party drone management software providers, governments, and authorities interested in using these data with the operator's authorization.